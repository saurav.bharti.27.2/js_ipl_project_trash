
const fs = require('fs-extra')

const delivery_data_promise = require('./deliveries_data')

const matches_data_promise = require('./match_data')



async function main(){
    try {
        const data_deliveries = await delivery_data_promise.then((data)=> {return data});
        const data_matches = await matches_data_promise.then((data => {return data;}))

        // total_no_of_match_played(data_matches)
        no_of_match_won_per_team_per_year(data_matches)
        // extra_run_conceded_by_each_team_in_year_2016(data_deliveries, data_matches)
        // top_10_economical_bowlers_in_2015(data_deliveries, data_matches)
        // number_of_times_a_team_won_toss_and_also_the_match(data_matches)
        // player_who_has_won_most_player_of_the_match_for_each_season(data_matches)
        // strike_rate_of_a_batsman_for_each_season(data_deliveries, data_matches)
        // highest_number_of_times_one_player_is_dismissed_by_other_player(data_deliveries)
        // bowler_with_best_economy_in_super_overs(data_deliveries)

    }catch(err){
        console.log(err.stack)
    }
}


function total_no_of_match_played(data){
    
    let cnt=0;
    let s = new Set()
    for(let i=1;i<data.length;i++){
        if(!s.has(data[i][0])){
            s.add(data[i][0])
            cnt+=1
        }
    }

    let address = '/home/saurav/practice/js_practice/ipl_draft/output/total_no_of_match_played.json'
    fs.outputJson( address, cnt).then(() => fs.readJson(address)).then(() => console.log(address)).catch(err => {console.log(err)})
    console.log(cnt)
}


function no_of_match_won_per_team_per_year(data){
    let matches_per_year_by_each_team = {}

    for(let i=1;i<data.length; i++){
        let season = data[i][1], team1 = data[i][4], team2 = data[i][5] 

        if(!matches_per_year_by_each_team[season]){
            matches_per_year_by_each_team[season] = {}
        }


        if(!matches_per_year_by_each_team[season][team1]){
            matches_per_year_by_each_team[season][team1] = 1
        }else{
            matches_per_year_by_each_team[season][team1] += 1
        }

        if(!matches_per_year_by_each_team[season][team2]){
            matches_per_year_by_each_team[season][team2] = 1
        }else{
            matches_per_year_by_each_team[season][team2] += 1
        }
        
    }

    let address = '/home/saurav/practice/js_practice/ipl_draft/output/no_of_match_won_per_team_per_year.json'
    fs.outputJson( address, matches_per_year_by_each_team).then(() => fs.readJson(address)).then(() => console.log(address)).catch(err => {console.log(err)})

    console.log(matches_per_year_by_each_team)

}

function extra_run_conceded_by_each_team_in_year_2016(delivery_data, match_data){

    let by_each_team_extra_run_conceded_in_2016 = {}
    console.log(delivery_data.length, match_data.length)

    for(let i=1; i<delivery_data.length; i++){

        let match_id = parseInt(delivery_data[i][0]);

        let season = match_data[match_id][1]; 

        if(season === '2016'){

            let bowling_team = delivery_data[i][3];
    
            let extra_run_conceded = delivery_data[i][16];
    
            if(!by_each_team_extra_run_conceded_in_2016[bowling_team]){
                by_each_team_extra_run_conceded_in_2016[bowling_team] = 0
            }
    
            if(parseInt(extra_run_conceded)){
                by_each_team_extra_run_conceded_in_2016[bowling_team] += parseInt(extra_run_conceded)
            }
        }

    }

    console.log(by_each_team_extra_run_conceded_in_2016)

}

function top_10_economical_bowlers_in_2015(delivery_data, match_data){
    
    let best_10_economical_bowlers_in_2015 = []
    let economy_of_bowlers_in_2015 = {}

    for(let i=1;i<delivery_data.length; i++){

        let match_id = parseInt(delivery_data[i][0]);

        let season = match_data[match_id][1];
        
        if(season === '2015'){

            let bowler = delivery_data[i][8]
    
            let total_runs_on_a_ball = parseInt(delivery_data[i][17])

            let is_wide = parseInt(delivery_data[i][10])
            
            let is_no_ball = parseInt(delivery_data[i][13]);

            if(!economy_of_bowlers_in_2015[bowler]){

                economy_of_bowlers_in_2015[bowler] = [0, 0]

            }
            let [total_ball_bowled_by_him, runs_conceded] = economy_of_bowlers_in_2015[bowler]

            economy_of_bowlers_in_2015[bowler] = [ total_ball_bowled_by_him + ( (is_wide || is_no_ball) ? 0 : 1) , runs_conceded + total_runs_on_a_ball ]

        }

    }

    // Not to count balls - Wide, No ball,

    let bowlers_by_their_economy = []

    for( let bowler in economy_of_bowlers_in_2015){

        bowlers_by_their_economy.push([ (economy_of_bowlers_in_2015[bowler][1]/economy_of_bowlers_in_2015[bowler][0] * 6) , bowler ])
    }

    bowlers_by_their_economy = bowlers_by_their_economy.sort((bowler_a, bowler_b) => (bowler_a[0] - bowler_b[0]))

    for(let i=0;i<10;i++){
        best_10_economical_bowlers_in_2015.push(bowlers_by_their_economy[i])
    }

    console.log(best_10_economical_bowlers_in_2015)

}

function number_of_times_a_team_won_toss_and_also_the_match(match_data){

    let total_number_of_times_a_team_won_toss_and_also_the_match = 0;

    for(let i=1; i<match_data.length; i++){

        const team_who_won_the_toss = match_data[i][6]

        const team_who_won_the_match = match_data[i][10]

        if(team_who_won_the_match === team_who_won_the_toss){

            total_number_of_times_a_team_won_toss_and_also_the_match += 1

        }

    }

    console.log(total_number_of_times_a_team_won_toss_and_also_the_match)

}

function player_who_has_won_most_player_of_the_match_for_each_season(match_data){

    let most_player_of_the_match_winners_by_each_season = []

    frequency_of_pom_by_each_season = {}

    for(let i=1; i< match_data.length; i++){

        let season = match_data[i][1]

        let player_of_match = match_data[i][13]


        if(!frequency_of_pom_by_each_season[season]){

            frequency_of_pom_by_each_season[season] = {}

        }

        if( !frequency_of_pom_by_each_season[season][player_of_match] ){

            frequency_of_pom_by_each_season[season][player_of_match] = 1

        }else{

            frequency_of_pom_by_each_season[season][player_of_match] += 1

        }

    }

    for( const season in frequency_of_pom_by_each_season){

        let mx= -100000000

        let most_player_of_match_winners_for_the_season 

        for( const good_players in frequency_of_pom_by_each_season[season] ){
            
            if(frequency_of_pom_by_each_season[season][good_players] > mx){

                mx = frequency_of_pom_by_each_season[season][good_players]

                most_player_of_match_winners_for_the_season = good_players

            }
        }

        most_player_of_the_match_winners_by_each_season.push([ season , most_player_of_match_winners_for_the_season ])

    }

    console.log(most_player_of_the_match_winners_by_each_season)

}


function strike_rate_of_a_batsman_for_each_season(delivery_data, match_data){

    let strike_rate_per_season_for_a_batsman = {}

    let balls_played_vs_scored_for_each_batsman_per_season = {}

    for( let i = 1; i<delivery_data.length; i++){
        
        let match_id = parseInt(delivery_data[i][0]);

        let season = match_data[match_id][1];

        let batsman = delivery_data[i][6]

        // console.log(batsman, season)

        if(!balls_played_vs_scored_for_each_batsman_per_season[batsman]){
            balls_played_vs_scored_for_each_batsman_per_season[batsman] = {}
        }

        if( !balls_played_vs_scored_for_each_batsman_per_season[batsman][season]){
            balls_played_vs_scored_for_each_batsman_per_season[batsman][season] = [0, 0]
        }

        let runs_scored_by_batsman = parseInt(delivery_data[i][15])

        let is_wide = parseInt(delivery_data[i][10])

        let is_no_ball = parseInt(delivery_data[i][13]);

        let [balls_played_by_the_batsman, total_run_of_his] = balls_played_vs_scored_for_each_batsman_per_season[batsman][season]

        balls_played_vs_scored_for_each_batsman_per_season[batsman][season] = [ ((is_wide || is_no_ball) ? 0 : 1) + balls_played_by_the_batsman , runs_scored_by_batsman + total_run_of_his  ];

    }


    for(const batsman in balls_played_vs_scored_for_each_batsman_per_season){

        for(const season_played_by_him in balls_played_vs_scored_for_each_batsman_per_season[batsman]){

            if(!strike_rate_per_season_for_a_batsman[batsman]){
                strike_rate_per_season_for_a_batsman[batsman] = {}
            }

            if(!strike_rate_per_season_for_a_batsman[batsman][season_played_by_him]){


                strike_rate_per_season_for_a_batsman[batsman][season_played_by_him] = ( balls_played_vs_scored_for_each_batsman_per_season[batsman][season_played_by_him][1]/balls_played_vs_scored_for_each_batsman_per_season[batsman][season_played_by_him][0] ) * 100 
            }
        }
    }

    console.log(strike_rate_per_season_for_a_batsman)
    
}

function highest_number_of_times_one_player_is_dismissed_by_other_player ( delivery_data ){

    let all_bowlers_who_dismissed_a_batsman = {}

    let most_times_one_bowler_got_out_one_batsman = {}

    for (let i=1; i<delivery_data.length; i++){

        let is_dismissed = delivery_data[i][18]

        let dismissal_type = delivery_data[i][19]

        let batsman= delivery_data[i][6]

        let bowler = delivery_data[i][8]

        // console.log(is_dismissed, dismissal_type, batsman, bowler)

        if(is_dismissed != ''){

            if(dismissal_type !== 'run out'){
    
                if(!all_bowlers_who_dismissed_a_batsman[batsman]){
    
                    all_bowlers_who_dismissed_a_batsman[batsman] = {}
                }
    
                if(!all_bowlers_who_dismissed_a_batsman[batsman][bowler]){
                    all_bowlers_who_dismissed_a_batsman[batsman][bowler] = 0
                }
    
                all_bowlers_who_dismissed_a_batsman[batsman][bowler] += 1
    
            }

        }

    }

    // console.log(all_bowlers_who_dismissed_a_batsman)

    let maximum = -10000000

    for(const batsman in all_bowlers_who_dismissed_a_batsman){



        for(const bowlers in all_bowlers_who_dismissed_a_batsman[batsman]){

            if(all_bowlers_who_dismissed_a_batsman[batsman][bowlers] > maximum){

                maximum = all_bowlers_who_dismissed_a_batsman[batsman][bowlers]

                most_times_one_bowler_got_out_one_batsman = [ maximum, batsman, bowlers ]

            }
        }

    }

    console.log(most_times_one_bowler_got_out_one_batsman)

}


function bowler_with_best_economy_in_super_overs( delivery_data ){

    let bowler_with_minimum_economy_in_super_overs;

    let bowler_with_super_overs_ball_and_runs = {}

    for(let i =1; i< delivery_data.length; i++){
        
        let bowler = delivery_data[i][8]

        let super_over = parseInt(delivery_data[i][9])

        let is_wide = parseInt(delivery_data[i][10])

        let is_no_ball = parseInt(delivery_data[i][13])

        let total_runs = parseInt(delivery_data[i][17])

        if(super_over){

            if(!bowler_with_super_overs_ball_and_runs[bowler]){
                bowler_with_super_overs_ball_and_runs[bowler] = [0, 0]
            }

            const [balls_bowled_by_him, runs_conceded] = bowler_with_super_overs_ball_and_runs[bowler]

            bowler_with_super_overs_ball_and_runs[bowler] = [ ((is_wide || is_no_ball) ? 0 : 1) + balls_bowled_by_him, total_runs+runs_conceded ]

        }
    }

    let minimum = 10000000

    for(const bowler in bowler_with_super_overs_ball_and_runs){

        
        let his_economy = (bowler_with_super_overs_ball_and_runs[bowler][1] / bowler_with_super_overs_ball_and_runs[bowler][0] ) * 6

        console.log(bowler, bowler_with_super_overs_ball_and_runs[bowler] , his_economy)

        if(his_economy < minimum){

            minimum = his_economy;

            bowler_with_minimum_economy_in_super_overs = bowler

        }
    }
    
    console.log(bowler_with_minimum_economy_in_super_overs, minimum)

}




/// running the show from here ----
main()