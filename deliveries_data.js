
const fs = require('fs');
const csv = require('@fast-csv/parse');

const my_data_of_deliveries= []

function do_it(){

    return new Promise((resolve, reject) =>{

        fs.createReadStream('deliveries.csv')
            .pipe(csv.parse())
            .on('error', error => reject(error))
            .on('data', row => {
                
                my_data_of_deliveries.push(row)
            }).on('end', () => resolve())
            ;
    })
}

const delivery_data = do_it();

module.exports = delivery_data.then(()=> my_data_of_deliveries)
// trigger()

// console.log("hey non blocking")

// module.exports = my_data_of_deliveries