
const fs = require('fs');
const csv = require('@fast-csv/parse');

const my_data_of_matches = []

function do_it(){

    return new Promise((resolve, reject) =>{

        fs.createReadStream('matches.csv')
            .pipe(csv.parse())
            .on('error', error => reject(error))
            .on('data', row => {
                
                my_data_of_matches.push(row)
            }).on('end', () => resolve())
            ;
    })
}

// async function trigger(){
//     try{
//         await do_it()
//         console.log("-hey-", my_data_of_matches.length , " -hey- ")

//     }catch(err){
//         console.log(err.stack)
//     }
// }
// trigger()

const matches_data = do_it()

module.exports =  matches_data.then(()=> my_data_of_matches)

